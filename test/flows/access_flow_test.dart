import 'dart:convert';
import 'dart:io';

import 'package:classic_blue_flutter_app/components/display_photo.dart';
import 'package:classic_blue_flutter_app/dependency_wrapper/native_screenshot_wrapper.dart';
import 'package:classic_blue_flutter_app/main.dart';
import 'package:classic_blue_flutter_app/screens/confirm_capture/confirm_capture.dart';
import 'package:classic_blue_flutter_app/screens/feed/feed.dart';
import 'package:classic_blue_flutter_app/screens/photo_detail/photo_detail.dart';
import 'package:classic_blue_flutter_app/screens/qr_code_tutorial/qr_code_tutorial.dart';
import 'package:classic_blue_flutter_app/screens/see_on_the_wall/see_on_the_wall.dart';
import 'package:classic_blue_flutter_app/screens/welcome/welcome.dart';
import 'package:classic_blue_flutter_app/services/api/unsplash_api.dart';
import 'package:classic_blue_flutter_app/services/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:network_image_mock/network_image_mock.dart';
import 'package:path/path.dart';

import '../mocks/access_flow.mocks.dart';

@GenerateMocks([http.Client, NativeScreenshotWrapper])
void main() {
  Directory directory;

  setUpAll(() async {
    // Create a temporary directory.
    directory = await Directory.systemTemp.createTemp();
  });

  testWidgets(
      'Should access feed, photo detail, qrcode tutorial, see on the wall and capture an image',
      (WidgetTester tester) async {
    mockNetworkImagesFor(() async {
      // Configure mocks

      client = MockClient();

      nativeScreenshot = MockNativeScreenshotWrapper();

      final mapJson = [
        {
          "height": 4500,
          "urls": {
            "regular":
                "https://images.unsplash.com/photo-1601925059835-3c6655cdb1f3?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjEzNTc0M30",
          },
          "width": 3000,
          'user': {
            'name': 'Jordon Wells',
            'profile_image': {
              "small":
                  "https://images.unsplash.com/profile-1597066412192-bdb46e127e63image?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=32&w=32"
            }
          },
          "description": 'Shot by @wellsjordon Model @jorvenbyrd}'
        }
      ];

      final Uri photoUri = Uri.parse('$unsplashUrl/photos');

      when(client.get(photoUri))
          .thenAnswer((_) async => http.Response(json.encode(mapJson), 200));

      when(nativeScreenshot.takeScreenshot()).thenAnswer((_) async {
        String screenshot = "test_resources/screenshot.jpeg";

        String tmpScreenshot = join(directory.path,
            'screenshot_${DateTime.now().millisecondsSinceEpoch}.jpeg');

        File(screenshot).copySync(tmpScreenshot);
        return tmpScreenshot;
      });

      // Instanciate MyApp
      await tester.pumpWidget(MyApp());

      // Check if is in Welcome Screen
      final welcome = find.byType(Welcome);

      expect(welcome, findsOneWidget);

      // Navigate to Feed Screen

      final enterButton = find.widgetWithText(
        OutlinedButton,
        "ENTRAR",
      );

      expect(enterButton, findsOneWidget);
      await tester.tap(enterButton);
      await tester.pumpAndSettle();

      // Check if is in Feed Screen

      final feed = find.byType(Feed);

      expect(feed, findsOneWidget);

      // Navigate to PhotoDetail Screen

      final feedImage = find.byWidgetPredicate((widget) {
        if (widget is InkWell) {
          if (widget.child is DisplayPhoto) {
            return true;
          }
        }
        return false;
      });

      expect(feedImage, findsOneWidget);
      await tester.tap(feedImage);
      await tester.pumpAndSettle();

      // Check if is in Photo Detail Screen

      final photoDetail = find.byType(PhotoDetail);
      expect(photoDetail, findsOneWidget);

      // Navigate to QR Code Tutorial

      final seeOnTheWallAction = find.byKey(Key('action-VER NA PAREDE'));
      expect(seeOnTheWallAction, findsOneWidget);
      await tester.tap(seeOnTheWallAction);
      await tester.pumpAndSettle();

      // Check if is in QR Code Tutorial

      var qrCodeTutorial = find.byType(QRCodeTutorial);
      expect(qrCodeTutorial, findsOneWidget);

      // Navigate to See On The Wall Screen

      final advanceAction = find.byKey(Key('advance-button'));
      expect(advanceAction, findsOneWidget);
      await tester.tap(advanceAction);
      await tester.pumpAndSettle();

      // Check if is in See On The Wall Screen

      var seeOnTheWall = find.byType(SeeOnTheWall);
      expect(seeOnTheWall, findsOneWidget);

      // Take picture and navigate to Confirm Capture Screen

      var takePhotoButton = find.byKey(Key('take-photo-button'));
      expect(takePhotoButton, findsOneWidget);
      await tester.tap(takePhotoButton);
      await tester.pumpAndSettle();

      // Check if is in Confirm Capture Screen

      final confirmCapture = find.byType(ConfirmCapture);
      expect(confirmCapture, findsOneWidget);

      // Test discard action

      final discardButton = find.byType(OutlinedButton);
      expect(discardButton, findsOneWidget);
      await tester.tap(discardButton);
      await tester.pumpAndSettle();

      // Check if is back on See On The Wall Screen

      seeOnTheWall = find.byType(SeeOnTheWall);
      expect(seeOnTheWall, findsOneWidget);

      // Take another picture and navigate to Confirm Capture Screen again

      takePhotoButton = find.byKey(Key('take-photo-button'));
      expect(takePhotoButton, findsOneWidget);
      await tester.tap(takePhotoButton);
      await tester.pumpAndSettle();

      // Test save action

      final saveButton = find.byKey(Key('save-button'));
      expect(saveButton, findsOneWidget);
      await tester.tap(saveButton);
      await tester.pumpAndSettle();

      // Check if is back on See On The Wall Screen

      seeOnTheWall = find.byType(SeeOnTheWall);
      expect(seeOnTheWall, findsOneWidget);
    });
  });
}
