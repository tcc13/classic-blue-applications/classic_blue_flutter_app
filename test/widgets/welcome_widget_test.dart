import 'package:classic_blue_flutter_app/screens/welcome/welcome.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Should display logo and two buttons',
      (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(home: Welcome()));

    final logo = find.byType(Image);
    final createAccountButton = find.byType(ElevatedButton);
    final enterButton = find.byType(OutlinedButton);

    expect(logo, findsOneWidget);
    expect(createAccountButton, findsWidgets);
    expect(enterButton, findsOneWidget);
  });
}
