import 'package:flutter/cupertino.dart';

class Dimension {
  final double width;
  final double height;

  Dimension({@required this.width, @required this.height});

  @override
  String toString() {
    return 'Dimension{width: $width, height: $height}';
  }
}
