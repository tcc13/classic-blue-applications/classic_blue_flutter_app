class Photo {
  final int width;
  final int height;
  final String url;
  final String userName;
  final String userProfileImage;
  final String description;

  Photo(this.width, this.height, this.url, this.userName, this.userProfileImage,
      this.description);

  @override
  String toString() {
    return 'Photo{width: $width, height: $height, url: $url, userName: $userName, userProfileImage: $userProfileImage, description: $description}';
  }

  Photo.fromJson(Map<String, dynamic> json)
      : width = json['width'],
        height = json['height'],
        url = json['urls']['regular'],
        userName = json['user']['name'],
        userProfileImage = json['user']['profile_image']['small'],
        description = json['description'] != null ? json['description'] : "";
}
