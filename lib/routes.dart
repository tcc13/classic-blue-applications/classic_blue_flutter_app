import 'package:classic_blue_flutter_app/screens/feed/feed.dart';
import 'package:classic_blue_flutter_app/screens/photo_detail/photo_detail.dart';
import 'package:classic_blue_flutter_app/screens/qr_code_tutorial/qr_code_tutorial.dart';
import 'package:classic_blue_flutter_app/screens/see_on_the_wall/see_on_the_wall.dart';
import 'package:classic_blue_flutter_app/screens/confirm_capture/confirm_capture.dart';
import 'package:flutter/widgets.dart';
import 'package:classic_blue_flutter_app/screens/welcome/welcome.dart';

final Map<String, WidgetBuilder> routes = <String, WidgetBuilder>{
  "/": (BuildContext context) => Welcome(),
  "/Feed": (BuildContext context) => Feed(),
  "/PhotoDetail": (BuildContext context) => PhotoDetail(),
  "/SeeOnTheWall": (BuildContext context) => SeeOnTheWall(),
  "/ConfirmCapture": (BuildContext context) => ConfirmCapture(),
  "/QRCodeTutorial": (BuildContext context) => QRCodeTutorial()
};
