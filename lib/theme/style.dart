import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

ThemeData appTheme(BuildContext context) {
  return ThemeData(
    visualDensity: VisualDensity.adaptivePlatformDensity,
    primaryColor: Color(0xff0f4c81),
    accentColor: Color(0xff9fb7cd),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(Color(0xff0f4c81)),
        textStyle: MaterialStateProperty.all(
          TextStyle(
            color: Colors.white,
          ),
        ),
        padding: MaterialStateProperty.all(
          EdgeInsets.all(16),
        ),
      ),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: ButtonStyle(
        side: MaterialStateProperty.all(
          BorderSide(color: Color(0xff0f4c81)),
        ),
        foregroundColor: MaterialStateProperty.all(Color(0xff0f4c81)),
        padding: MaterialStateProperty.all(
          EdgeInsets.all(16),
        ),
      ),
    ),
    // buttonTheme: ButtonThemeData(
    //   buttonColor: Color(0xff0f4c81),
    //   height: 48,
    //   textTheme: ButtonTextTheme.primary,
    // ),
    textTheme: GoogleFonts.robotoTextTheme(Theme.of(context).textTheme)
        .copyWith(
            bodyText1: TextStyle(color: Color(0xff0f4c81)),
            bodyText2: TextStyle(color: Color(0xff0f4c81))),
    appBarTheme: AppBarTheme(
      textTheme:
          GoogleFonts.robotoTextTheme(Theme.of(context).textTheme).copyWith(
        headline6: TextStyle(
          color: Colors.white,
          fontSize: 18,
          fontWeight: FontWeight.w600,
        ),
      ),
      color: Color(0xff9fb7cd),
    ),
  );
}
