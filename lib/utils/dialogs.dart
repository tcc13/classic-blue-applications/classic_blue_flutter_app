import 'package:flutter/material.dart';

class Dialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return new WillPopScope(
            child: SimpleDialog(
              key: key,
              children: [
                Center(
                  child: Column(
                    children: [
                      CircularProgressIndicator(),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Aguarde um instante...',
                      )
                    ],
                  ),
                )
              ],
            ),
            onWillPop: () async => false,
          );
        });
  }

  static void closeDialog(GlobalKey key) {
    Navigator.of(key.currentContext, rootNavigator: true).pop();
  }
}
