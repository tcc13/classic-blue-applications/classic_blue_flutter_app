import 'package:classic_blue_flutter_app/routes.dart';
import 'package:classic_blue_flutter_app/theme/style.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: appTheme(context),
      initialRoute: "/",
      routes: routes,
    );
  }
}
