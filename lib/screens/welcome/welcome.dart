import 'package:flutter/material.dart';

final String createAccountButtonText = 'CRIAR CONTA';
final String enterAccountButtonText = 'ENTRAR';

class Welcome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
                child: Image.asset('assets/icon/classic_blue_logo_cropped.png'),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
            child: ElevatedButton(
              onPressed: () {},
              child: Text(createAccountButtonText),
            ),
          ),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
              child: OutlinedButton(
                child: Text(enterAccountButtonText),
                onPressed: () {
                  Navigator.pushNamed(context, '/Feed');
                },
              )),
        ],
      ),
    );
  }
}
