import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:charcode/charcode.dart';
import 'package:classic_blue_flutter_app/models/photo.dart';
import 'package:classic_blue_flutter_app/screens/see_on_the_wall/see_on_the_wall.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';

class QRCodeTutorialArguments {
  final Photo photo;

  QRCodeTutorialArguments(this.photo);
}

class QRCodeTutorial extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final QRCodeTutorialArguments args =
        ModalRoute.of(context).settings.arguments;

    return Scaffold(
        appBar: AppBar(
          title: Text('Tutorial'),
        ),
        body: Column(
          children: [
            Expanded(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(
                          "Tenha em mãos o código QR abaixo",
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text(
                                '${String.fromCharCode($bull)} Clique em obter código QR para imprimir ou compartilhar com um outro dispositivo'),
                            Text(
                                '${String.fromCharCode($bull)} Posicione o código QR onde deseja visualizar a imagem'),
                            Text(
                                "${String.fromCharCode($bull)} Por fim, clique em avançar e aponte a câmera para o código QR próximo a ele")
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: FractionallySizedBox(
                      widthFactor: 0.5,
                      heightFactor: 0.5,
                      child: Image.asset(
                        'assets/images/QRCode.png',
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    padding: EdgeInsets.only(bottom: 24),
                    child: OutlinedButton(
                      key: Key('advance-button'),
                      child: Text('AVANÇAR'),
                      onPressed: () {
                        Navigator.pushNamed(context, "/SeeOnTheWall",
                            arguments: SeeOnTheWallArguments(args.photo));
                      },
                    ),
                  ),
                  ElevatedButton(
                    key: Key('get-qr-code-button'),
                    onPressed: () async {
                      final ByteData bytes =
                          await rootBundle.load('assets/images/QRCode.png');
                      final Uint8List list = bytes.buffer.asUint8List();

                      final tempDir = await getTemporaryDirectory();
                      final file =
                          await new File('${tempDir.path}/image.jpg').create();
                      file.writeAsBytesSync(list);

                      Share.shareFiles(['${file.path}']);
                    },
                    child: Text('OBTER CÓDIGO QR'),
                  )
                ],
              ),
            )
          ],
        ));
  }
}
