import 'package:classic_blue_flutter_app/components/display_photo.dart';
import 'package:classic_blue_flutter_app/components/photo_user_info.dart';
import 'package:classic_blue_flutter_app/models/photo.dart';
import 'package:classic_blue_flutter_app/screens/photo_detail/photo_detail.dart';
import 'package:classic_blue_flutter_app/services/api/unsplash_api.dart';
import 'package:flutter/material.dart';

class Feed extends StatelessWidget {
  final UnsplashApi _unsplashApi = UnsplashApi();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextField(
          style: TextStyle(color: Colors.white),
          textAlign: TextAlign.center,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: 'Procure fotos ou artistas',
            hintStyle: TextStyle(
              color: Colors.white.withOpacity(0.7),
            ),
          ),
        ),
        leading: Icon(Icons.account_circle),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 8),
            child: Icon(Icons.search),
          )
        ],
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FeedCategories(),
          Expanded(
            child: FutureBuilder<List<Photo>>(
              future: _unsplashApi.findFeedPhotos(),
              builder: _feedBuilder,
            ),
          )
        ],
      ),
    );
  }

  Widget _feedBuilder(context, snapshot) {
    switch (snapshot.connectionState) {
      case ConnectionState.none:
        break;
      case ConnectionState.waiting:
        return Text('Loading');
        break;
      case ConnectionState.active:
        break;
      case ConnectionState.done:
        if (snapshot.hasData) {
          final List<Photo> photos = snapshot.data;
          if (photos.isNotEmpty) {
            return ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: 16),
                itemCount: photos.length,
                itemBuilder: (context, index) {
                  final Photo photo = photos[index];
                  return FeedItem(photo);
                });
          }
        }
        break;
    }
    return Text('Unknown error');
  }
}

class FeedCategories extends StatefulWidget {
  @override
  _FeedCategoriesState createState() => _FeedCategoriesState();
}

class _FeedCategoriesState extends State<FeedCategories> {
  final List<String> _categories = [
    'ALL',
    'TRENDING',
    'HOT',
    'SPONSORED',
  ];

  int _selectedCategory = 0;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 72,
      child: ListView.builder(
          padding: EdgeInsets.symmetric(horizontal: 4, vertical: 24),
          scrollDirection: Axis.horizontal,
          itemCount: _categories.length,
          itemBuilder: (context, index) {
            final String category = _categories[index];
            final bool isSelectedCategory = _selectedCategory == index;
            return FeedCategory(category, isSelectedCategory, () {
              setState(() {
                _selectedCategory = index;
              });
            });
          }),
    );
  }
}

class FeedCategory extends StatelessWidget {
  final String text;
  final bool isSelectedCategory;
  final Function onPress;

  FeedCategory(this.text, this.isSelectedCategory, this.onPress);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: Text(
          text,
          style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w600,
              color: isSelectedCategory
                  ? Theme.of(context).primaryColor
                  : Theme.of(context).accentColor),
        ),
      ),
    );
  }
}

class FeedItem extends StatelessWidget {
  final Photo photo;

  FeedItem(this.photo);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 24),
      child: Column(
        children: [
          FeedItemHeader(photo),
          InkWell(
            onTap: () {
              Navigator.pushNamed(context, "/PhotoDetail",
                  arguments: PhotoDetailArguments(photo));
            },
            child: DisplayPhoto(photo),
          ),
          Row(
            children: [
              FeedItemVoteButton(Icons.arrow_downward, VoteButtonSide.left),
              FeedItemVoteButton(Icons.arrow_upward, VoteButtonSide.right),
            ],
          ),
        ],
      ),
    );
  }
}

class FeedItemHeader extends StatefulWidget {
  final Photo photo;

  FeedItemHeader(this.photo);

  @override
  _FeedItemHeaderState createState() => _FeedItemHeaderState();
}

class _FeedItemHeaderState extends State<FeedItemHeader> {
  bool _savedItem = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: Row(
        children: [
          Expanded(child: PhotoUserInfo(widget.photo)),
          InkWell(
            onTap: () {
              setState(() {
                _savedItem = !_savedItem;
              });
            },
            child: Icon(
              _savedItem ? Icons.bookmark : Icons.bookmark_border,
              color: Theme.of(context).primaryColor,
            ),
          ),
        ],
      ),
    );
  }
}

enum VoteButtonSide { left, right }

class FeedItemVoteButton extends StatelessWidget {
  final IconData icon;
  final VoteButtonSide side;

  FeedItemVoteButton(this.icon, this.side);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: () {},
        child: Container(
          padding: EdgeInsets.all(8),
          child: Icon(
            icon,
            color: Theme.of(context).primaryColor,
          ),
          decoration: BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
          ),
        ),
      ),
    );
  }
}
