import 'dart:io';

import 'package:classic_blue_flutter_app/dependency_wrapper/native_screenshot_wrapper.dart';
import 'package:classic_blue_flutter_app/models/photo.dart';
import 'package:classic_blue_flutter_app/screens/confirm_capture/confirm_capture.dart';
import 'package:classic_blue_flutter_app/screens/see_on_the_wall/ar_view.dart';
import 'package:classic_blue_flutter_app/screens/see_on_the_wall/dimension_converter.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image/image.dart' as image;

NativeScreenshotWrapper nativeScreenshot = NativeScreenshotWrapper();

class SeeOnTheWallArguments {
  final Photo photo;

  SeeOnTheWallArguments(this.photo);
}

class SeeOnTheWall extends StatefulWidget {
  @override
  _SeeOnTheWallState createState() => _SeeOnTheWallState();
}

class _SeeOnTheWallState extends State<SeeOnTheWall> {
  int _selectedPaperSize = 1;

  final _arViewKey = GlobalKey();
  final _actionsKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  _capturePng(BuildContext context) async {
    String path = await nativeScreenshot.takeScreenshot();
    final RenderBox renderBoxRed = _arViewKey.currentContext.findRenderObject();
    final sizeArView = renderBoxRed.size;

    final RenderBox renderBoxActions =
        _actionsKey.currentContext.findRenderObject();
    final sizeActions = renderBoxActions.size;

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    final headerHeight = height - sizeActions.height - sizeArView.height;

    if (path == null || path.isEmpty) {
      return;
    } // if error

    File imgFile = File(path);

    final decodedImage = image.decodeImage(imgFile.readAsBytesSync());

    final widthRatio = decodedImage.width / width;

    final heightRatio = decodedImage.height / height;

    final croppedImage = image.copyCrop(
        decodedImage,
        0,
        (headerHeight * heightRatio).round(),
        (sizeArView.width * widthRatio).round(),
        (sizeArView.height.round() * heightRatio).round());

    File(path).writeAsBytesSync(image.encodePng(croppedImage));

    File finalFile = File(path);

    Navigator.pushNamed(context, "/ConfirmCapture",
        arguments: ConfirmCaptureArguments(finalFile));

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final SeeOnTheWallArguments args =
        ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text('Ver na parede'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ArView(
            key: _arViewKey,
            photo: args.photo,
            paperSize: PaperSize.values[_selectedPaperSize],
          ),
          Container(
            key: _actionsKey,
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PaperSizeSelector(
                  selectedPaperSize: _selectedPaperSize,
                  setSelectPaperSize: (int index) {
                    setState(() {
                      _selectedPaperSize = index;
                    });
                  },
                ),
                TakePhotoButton(
                  onTakePhoto: () {
                    _capturePng(context);
                  },
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class PaperSizeSelector extends StatefulWidget {
  final int selectedPaperSize;
  final void Function(int) setSelectPaperSize;

  PaperSizeSelector(
      {Key key,
      @required this.selectedPaperSize,
      @required this.setSelectPaperSize})
      : super(key: key);

  @override
  _PaperSizeSelectorState createState() => _PaperSizeSelectorState();
}

class _PaperSizeSelectorState extends State<PaperSizeSelector> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 52,
      padding: EdgeInsets.only(bottom: 24),
      alignment: Alignment.center,
      child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: PaperSize.values.length,
          itemBuilder: (context, index) {
            final PaperSize paperSize = PaperSize.values[index];

            final bool isSelectedPaperSize = widget.selectedPaperSize == index;
            return PaperSizeSelectorItem(
              text: EnumToString.convertToString(paperSize),
              isSelectedPaperSize: isSelectedPaperSize,
              onSelectPaperSize: () {
                widget.setSelectPaperSize(index);
              },
            );
          }),
    );
  }
}

class PaperSizeSelectorItem extends StatelessWidget {
  final String text;
  final bool isSelectedPaperSize;
  final Function onSelectPaperSize;

  const PaperSizeSelectorItem(
      {Key key,
      @required this.text,
      @required this.isSelectedPaperSize,
      @required this.onSelectPaperSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: InkWell(
        onTap: onSelectPaperSize,
        child: Container(
            decoration: BoxDecoration(
                color: isSelectedPaperSize
                    ? Theme.of(context).primaryColor
                    : Colors.transparent,
                border: Border.all(color: Theme.of(context).primaryColor)),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 4),
              child: Text(
                text,
                style: TextStyle(
                    color: isSelectedPaperSize
                        ? Colors.white
                        : Theme.of(context).primaryColor),
              ),
            )),
      ),
    );
  }
}

class TakePhotoButton extends StatelessWidget {
  final Function onTakePhoto;
  const TakePhotoButton({Key key, @required this.onTakePhoto})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        borderRadius: BorderRadius.all(Radius.circular(36)),
        key: Key('take-photo-button'),
        onTap: onTakePhoto,
        child: Container(
          width: 72.0,
          height: 72.0,
          alignment: Alignment.center,
          decoration: new BoxDecoration(
              shape: BoxShape.circle,
              border:
                  Border.all(color: Theme.of(context).primaryColor, width: 4)),
          child: Container(
            width: 62.0,
            height: 62.0,
            decoration: new BoxDecoration(
              color: Theme.of(context).primaryColor,
              shape: BoxShape.circle,
            ),
          ),
        ),
      ),
    );
  }
}
