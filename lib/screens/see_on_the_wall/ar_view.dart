import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
import 'package:classic_blue_flutter_app/models/dimension.dart';
import 'package:classic_blue_flutter_app/models/photo.dart';
import 'package:classic_blue_flutter_app/screens/see_on_the_wall/dimension_converter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vector_math/vector_math_64.dart' as vector;

class ArView extends StatefulWidget {
  final Photo photo;
  final PaperSize paperSize;

  ArView({Key key, @required this.photo, @required this.paperSize})
      : super(key: key);

  @override
  _ArViewState createState() => _ArViewState();
}

class _ArViewState extends State<ArView> {
  ArCoreController arCoreController = ArCoreController();
  Map<int, ArCoreAugmentedImage> augmentedImagesMap = Map();

  @override
  Widget build(BuildContext context) {
    augmentedImagesMap.forEach((index, image) {
      arCoreController.removeNodeWithIndex(image.index);
    });

    augmentedImagesMap = Map();

    return Expanded(
      child: ArCoreView(
        onArCoreViewCreated: _onArCoreViewCreated,
        enablePlaneRenderer: false,
        enableTapRecognizer: false,
        enableUpdateListener: false,
        type: ArCoreViewType.AUGMENTEDIMAGES,
      ),
    );
  }

  void _onArCoreViewCreated(ArCoreController controller) async {
    arCoreController = controller;
    arCoreController.onTrackingImage = _onTrackingImage;

    loadSingleImage();
  }

  void loadSingleImage() async {
    final ByteData bytes = await rootBundle.load('assets/images/QRCode.png');
    arCoreController.loadSingleAugmentedImage(
        bytes: bytes.buffer.asUint8List());
  }

  void _onTrackingImage(ArCoreAugmentedImage image) async {
    if (!augmentedImagesMap.containsKey(image.index)) {
      augmentedImagesMap[image.index] = image;
      _addPhoto(image);
    }
  }

  void _addPhoto(ArCoreAugmentedImage image) async {
    final url = widget.photo.url;

    final photoAsUint8List =
        (await NetworkAssetBundle(Uri.parse(url)).load(url))
            .buffer
            .asUint8List();

    final photoMaterial = ArCoreMaterial(
        color: Color.fromARGB(0, 0, 0, 0),
        textureBytes: photoAsUint8List.buffer.asUint8List());

    final dimension = dimensionConverter(
      Dimension(
          width: widget.photo.width.toDouble(),
          height: widget.photo.height.toDouble()),
      widget.paperSize,
    );

    final earthShape = ArCoreCube(
      materials: [photoMaterial],
      size: vector.Vector3(dimension.width, 0.0, dimension.height),
    );

    final photoNode = ArCoreNode(
        shape: earthShape, position: vector.Vector3(0.0, -0.15, 0.0));

    arCoreController.addArCoreNodeToAugmentedImage(photoNode, image.index);
  }

  @override
  void dispose() {
    arCoreController.dispose();
    super.dispose();
  }
}
