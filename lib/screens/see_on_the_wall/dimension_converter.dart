import 'package:classic_blue_flutter_app/models/dimension.dart';

enum PaperSize { A0, A1, A2, A3, A4, A5 }

final Map<PaperSize, Dimension> paperSizeMap = <PaperSize, Dimension>{
  PaperSize.A0: Dimension(width: 1.189, height: 0.841),
  PaperSize.A1: Dimension(width: 0.841, height: 0.594),
  PaperSize.A2: Dimension(width: 0.594, height: 0.42),
  PaperSize.A3: Dimension(width: 0.42, height: 0.297),
  PaperSize.A4: Dimension(width: 0.297, height: 0.21),
  PaperSize.A5: Dimension(width: 0.21, height: 0.148),
};

bool isInLandscapeOrientation(Dimension dimension) {
  return dimension.width > dimension.height;
}

Dimension dimensionConverter(Dimension dimension, PaperSize paperSize) {
  double ratio;
  double paperSizeRatio;
  Dimension paperSizeDimension;
  Dimension realWorldDimension;

  if (isInLandscapeOrientation(dimension)) {
    // ratio, paperSizeDimension and paperSizeRatio for landscape orientation
    ratio = dimension.height / dimension.width;
    paperSizeDimension = paperSizeMap[paperSize];
    paperSizeRatio = paperSizeDimension.height / paperSizeDimension.width;

    // If true, dimension fits width, otherwise fits height
    if (ratio < paperSizeRatio) {
      realWorldDimension = Dimension(
        width: paperSizeDimension.width,
        height: paperSizeDimension.width * ratio,
      );
    } else {
      realWorldDimension = Dimension(
        width: paperSizeDimension.height / ratio,
        height: paperSizeDimension.height,
      );
    }
  } else {
    // ratio, paperSizeDimension and paperSizeRatio for portrait orientation
    ratio = dimension.width / dimension.height;
    // Change to portrait orientation
    paperSizeDimension = Dimension(
      height: paperSizeMap[paperSize].width,
      width: paperSizeMap[paperSize].height,
    );

    paperSizeRatio = paperSizeDimension.width / paperSizeDimension.height;

    // If true, dimension fits height, otherwise fits width
    if (ratio < paperSizeRatio) {
      realWorldDimension = Dimension(
        height: paperSizeDimension.height,
        width: paperSizeDimension.height * ratio,
      );
    } else {
      realWorldDimension = Dimension(
        width: paperSizeDimension.width,
        height: paperSizeDimension.width / ratio,
      );
    }
  }

  return realWorldDimension;
}
