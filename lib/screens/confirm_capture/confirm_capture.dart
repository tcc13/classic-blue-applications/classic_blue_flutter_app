import 'dart:io';

import 'package:flutter/material.dart';

class ConfirmCaptureArguments {
  final File photoFile;

  ConfirmCaptureArguments(this.photoFile);
}

class ConfirmCapture extends StatelessWidget {
  const ConfirmCapture({Key key}) : super(key: key);

  void _discardFile(File photoFile) {
    try {
      photoFile.deleteSync();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    final ConfirmCaptureArguments args =
        ModalRoute.of(context).settings.arguments;

    return WillPopScope(
      onWillPop: () {
        _discardFile(args.photoFile);
        Navigator.pop(context, true);
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('Salvar imagem'),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              constraints: BoxConstraints(minHeight: 1, minWidth: 1),
              child: Image.file(args.photoFile, fit: BoxFit.fitWidth),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    padding: EdgeInsets.only(bottom: 24),
                    child: OutlinedButton(
                      child: Text('DESCARTAR'),
                      onPressed: () {
                        _discardFile(args.photoFile);
                        Navigator.pop(context, true);
                      },
                    ),
                  ),
                  ElevatedButton(
                    key: Key('save-button'),
                    onPressed: () {
                      Navigator.pop(context, true);
                    },
                    child: Text('SALVAR'),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
