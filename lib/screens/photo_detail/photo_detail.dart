import 'package:classic_blue_flutter_app/components/display_photo.dart';
import 'package:classic_blue_flutter_app/components/photo_user_info.dart';
import 'package:classic_blue_flutter_app/models/photo.dart';
import 'package:classic_blue_flutter_app/screens/qr_code_tutorial/qr_code_tutorial.dart';
import 'package:flutter/material.dart';

class PhotoDetailArguments {
  final Photo photo;

  PhotoDetailArguments(this.photo);
}

class PhotoDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final PhotoDetailArguments args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text('Detalhe da foto'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 24, horizontal: 16),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: 12),
                child: PhotoUserInfo(args.photo),
              ),
              DisplayPhoto(args.photo),
              PhotoActions(args.photo),
              Padding(
                padding: const EdgeInsets.only(top: 12),
                child: Text(args.photo.description),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class PhotoActions extends StatelessWidget {
  final Photo _photo;

  PhotoActions(this._photo);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        PhotoAction(Icons.remove_red_eye, 'VER NA PAREDE', () {
          Navigator.pushNamed(context, "/QRCodeTutorial",
              arguments: QRCodeTutorialArguments(_photo));
        })
      ],
    );
  }
}

class PhotoAction extends StatelessWidget {
  final IconData icon;
  final String text;
  final Function onTap;

  PhotoAction(
    this.icon,
    this.text,
    this.onTap,
  );

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        key: Key('action-$text'),
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.all(8),
          child: Column(
            children: [
              Icon(
                icon,
                color: Theme.of(context).primaryColor,
              ),
              Text(text)
            ],
          ),
          decoration: BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
          ),
        ),
      ),
    );
  }
}
