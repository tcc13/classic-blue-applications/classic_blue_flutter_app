import 'package:native_screenshot/native_screenshot.dart';

class NativeScreenshotWrapper {
  Future<String> takeScreenshot() {
    return NativeScreenshot.takeScreenshot();
  }
}
