import 'package:http_interceptor/http_interceptor.dart';

final _unsplashAccessKey = 'BeTn_bBITtkEdaRHgXZYFTIGWShv14x-6_DfHJ1txcw';

class UnsplashInterceptor implements InterceptorContract {
  @override
  Future<RequestData> interceptRequest({RequestData data}) async {
    data.headers = {
      'Accept-Version': 'v1',
      'Authorization': 'Client-ID $_unsplashAccessKey',
    };
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({ResponseData data}) async {
    return data;
  }
}
