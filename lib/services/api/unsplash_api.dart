import 'dart:convert';

import 'package:classic_blue_flutter_app/models/photo.dart';
import 'package:classic_blue_flutter_app/services/services.dart';
import 'package:http/http.dart';

const String unsplashUrl = "https://api.unsplash.com";

class UnsplashApi {
  Future<List<Photo>> findFeedPhotos() async {
    final Uri photoUri = Uri.parse('$unsplashUrl/photos');

    final Response response = await client
        .get(photoUri)
        .timeout(Duration(seconds: 15));

    final List<dynamic> decodedJson = jsonDecode(response.body);

    return decodedJson.map((dynamic photo) => Photo.fromJson(photo)).toList();
  }
}
