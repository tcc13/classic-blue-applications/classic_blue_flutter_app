import 'package:classic_blue_flutter_app/services/interceptors/unsplash_interceptor.dart';
import 'package:http/http.dart';
import 'package:http_interceptor/http_interceptor.dart';

Client client = HttpClientWithInterceptor.build(
  interceptors: [UnsplashInterceptor()],
);
