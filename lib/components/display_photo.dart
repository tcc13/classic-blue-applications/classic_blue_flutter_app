import 'package:classic_blue_flutter_app/models/photo.dart';
import 'package:flutter/material.dart';

class DisplayPhoto extends StatelessWidget {
  final Photo photo;

  DisplayPhoto(this.photo);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          constraints: BoxConstraints(minHeight: 1, minWidth: 1),
          child: Image.network(
            photo.url,
            fit: BoxFit.fitWidth,
          ),
        ),
      ],
    );
  }
}