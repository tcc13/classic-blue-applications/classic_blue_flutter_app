import 'package:classic_blue_flutter_app/models/photo.dart';
import 'package:flutter/material.dart';

class PhotoUserInfo extends StatelessWidget {
  final Photo _photo;

  PhotoUserInfo(this._photo);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _photo.userProfileImage != "" ? Image.network(_photo.userProfileImage) :
        Icon(Icons.account_circle, color: Theme.of(context).primaryColor),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              _photo.userName,
              style: TextStyle(fontWeight: FontWeight.w600, ),
            ),
          ),
        )
      ],
    );
  }
}
